(ns blah.core
  (:gen-class))


(def num-lives (atom 5))

(defn make-vec [x y z] {:x x :y y :z})

(defn operate-vec [op v1 v2] 
  (let [{x1 :x y1 :y z1 :z} v1
        {x2 :x y2 :y z2 :z} v2 ]
    (make-vec (op x1 x2) (op y1 y2)  (op z1 z2))))

(def add-vec (partial operate-vec +))
(def subtract-vec (partial operate-vec -))
(def mult-vec (partial operate-vec *))
(defn scale-vec [v scalar] (mult-vec v (make-vec scalar scalar scalar)))

(defn normalize-vec [v]
  (let [{x :x y :y z :z} v]
  (scale-vec v (double (/ 1 (Math/sqrt (+ (* x x) (* y y) (* z z))))))))

(defn dot-vec [v1 v2] (apply + (vals (mult-vec v1 v2))))

(defn cross-vec [v1 v2] 
  (let [{x1 :x y1 :y z1 :z} v1
        {x2 :x y2 :y z2 :z} v2 
        x (- (* y1 z2) (* z1 y2))
        y (- (* z1 x2) (* x1 z2))
        z (- (* x1 y2) (* y1 x2)) ]
  (make-vec x y z))

; o and d are vecs
(defn make-ray [o d] {:o o :d d})

; p e c are vecs
(defn make-sphere [rad p e c refl] {:rad rad :p p :e e :refl refl})

(defn intersect [s r]
  (let [op (sub-vec (:p s) (:o r))
        eps 1e-4
        b (dot-vec op (:d r))
        det (+ (- (* b b) (dot-vec op op)) (* rad rad))
        ]
    (if (< det 0)
      0
      (let [det2 (Math/sqrt det)
            t1 (- b det2)
            t2 (+ b det2)
            ]
        (if (< t1 eps) t1 (if (> t2 eps) t 0))))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!")
  
  )
